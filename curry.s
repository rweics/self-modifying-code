.text
# Your code here. Remember that you need to turn on self-modifying mode in order for
# Mars to allow self-modifying mips.
# $a0 corresponds to destination buffer for the curryable-ized function
# $a1 corresponds to the function to be curryable-ized
# $a2 corresponds to the number of arguments to the function to be curryable-ized
curry:

# load address of destination to $v0

	# first line of destination
	li $t0, 0x3C020000 # lui $v0, 0
	move $t1, $a0
	srl $t1, $t1, 16
	addu $t0, $t0, $t1
	sw $t0, 0($a0)
	
	# second line of destination
	li $t0, 0x34420000 # ori $v0, $v0, 0
	move $t1, $a0
	andi $t1, $t1, 0x0000FFFF
	addu $t0, $t0, $t1
	sw $t0, 4($a0)
	
# load address of function to $t1	

	# third line of destination
	li $t0, 0x3C090000 # lui $t1, 0
	move $t1, $a1
	srl $t1, $t1, 16
	addu $t0, $t0, $t1
	sw $t0, 8($a0)
	
	# fourth line of destination
	li $t0, 0x35290000 # ori $ti, $t1, 0
	move $t1, $a1
	andi $t1, $t1, 0x0000FFFF
	addu $t0, $t0, $t1
	sw $t0, 12($a0)
	
# load number of arguments to $t2

	# fifth line of destination
	li $t0, 0x200A0000 # addi $t2, $zero, 0
	addu $t0, $t0, $a2
	sw $t0, 16($a0)
	
# load numCalled to $t3

	# sixth line of destination
	li $t0, 0x200B0000 # addi $t3, $zero, 0
	sw $t0, 20($a0)
	
# load first argument to $t4

	# seventh line of destination
	li $t0, 0x3C0C0000 # lui $t4, 0
	sw $t0, 24($a0)

	# eighth line of destination
	li $t0, 0x358C0000 # ori $t4, $t4, 0
	sw $t0, 28($a0)

# load second argument to $t5

	# nineth line of destination
	li $t0, 0x3C0D0000 # lui $t5, 0
	sw $t0, 32($a0)

	# tenth line of destination
	li $t0, 0x35AD0000 # ori $t5, $t5, 0
	sw $t0, 36($a0)

# load third argument to $t6

	# eleventh line of destination
	li $t0, 0x3C0E0000 # lui $t6, 0
	sw $t0, 40($a0)

	# twelfth line of destination
	li $t0, 0x35CE0000 # ori $t6, $t6, 0
	sw $t0, 44($a0)

# load fourth argument to $t7

	# thirteenth line of destination
	li $t0, 0x3C0F0000 # lui $t7, 0
	sw $t0, 48($a0)

	# fourteenth line of destination
	li $t0, 0x35EF0000 # ori $t7, $t7, 0
	sw $t0, 52($a0)

# jump to curried

	# fifteenth line of destination
	li $t0, 0x3C080000 # lui $t0, 0
	la $t1, curried
	srl $t1, $t1, 16
	addu $t0, $t0, $t1
	sw $t0, 56($a0)
	
	# sixteenth line of destination
	li $t0, 0x35080000 # ori $t0, $t0, 0
	la $t1, curried
	andi $t1, $t1, 0x0000FFFF
	addu $t0, $t0, $t1
	sw $t0, 60($a0)
	
	# seventeenth line of destination
	li $t0 0x01000008 # jr $t0
	sw $t0, 64($a0)
	
	# return
	jr $ra 

	curried:
		addi $t3, $t3, 1 # increment numCalled by 1
		lw $t0, 20($v0) # load numCalled instruction to memory
		addiu $t0, $t0, 1 # increment numCalled (memCopy) by 1
		sw $t0, 20($v0) # update numCalled to memory
		
		li $t0, 1 # load 1 to $t0
		beq $t0, $t3, first # if first time called
		li $t0, 2 # load 2 to $t0
		beq $t0, $t3, second # if second time called
		li $t0, 3 # load 3 to $t0
		beq $t0, $t3, third # if third time called
		li $t0, 4 # load 4 to $t0
		beq $t0, $t3, fourth # if fourth time called
		
		first:
		move $t4, $a0 # move $a0 to $t4
		# store upper instruction of arg1
		lw $t0, 24($v0)
		srl $a0, $a0, 16
		addu $t0, $t0, $a0
		sw $t0, 24($v0)
		# store lower instruction of arg1
		lw $t0, 28($v0) 
		move $a0, $t4
		andi $a0, $a0, 0x0000FFFF
		addu $t0, $t0, $a0
		sw $t0, 28($v0)
		j end
		
		second:
		move $t5, $a0 # move $a0 to $t5
		# store upper instruction of arg2
		lw $t0, 32($v0)
		srl $a0, $a0, 16
		addu $t0, $t0, $a0
		sw $t0, 32($v0)
		# store lower instruction of arg2
		lw $t0, 36($v0) 
		move $a0, $t5
		andi $a0, $a0, 0x0000FFFF
		addu $t0, $t0, $a0
		sw $t0, 36($v0)
		j end
		
		third:
		move $t6, $a0 # move $a0 to $t6
		# store upper instruction of arg3
		lw $t0, 40($v0)
		srl $a0, $a0, 16
		addu $t0, $t0, $a0
		sw $t0, 40($v0)
		# store lower instruction of arg3
		lw $t0, 44($v0)
		move $a0, $t6
		andi $a0, $a0, 0x0000FFFF
		addu $t0, $t0, $a0
		sw $t0, 44($v0)
		j end
		
		fourth:
		move $t7, $a0 # move $a0 to $t7
		
		end:
		beq $t2, $t3, call # if numCalled == numArgs, call original function
		jr $ra # else, return carried
		
		call:
		move $4, $t4
		move $5, $t5
		move $6, $t6
		move $7, $t7
		jr $t1
		

#This is the destination buffer we'll be using for your curryable-ized function.
#I found it helpful to scratch out what lines of code I'd be having my code
#write to this buffer before writing curry. Feel free to add more nops to this
#buffer if you need to, but know that your code should only need O(1) space for
#any function we hand you. If you find yourself wanting more than constant space
#you're probably doing something wrong.
destination:
	nop #
	nop #
        nop #
        nop #
        nop #

	nop #
	nop #
        nop #
        nop #
        nop #

	nop #
	nop #
        nop #
        nop #
        nop #

	nop #
	nop #
        nop #
        nop #
        nop #

        nop #
        nop #
        nop #
        nop #
        nop #
        nop #
        nop #
        nop #

        nop #
        nop #
        nop #
        nop #
        nop #
