from math import floor

def add3(x, y, z):
	return x + y + z

def mul2(x, y):
	return x * y

def show1(x):
	return floor(x / 2 + 1)

# Since python does not support self-modifying, instead of modifying destination, here curry returns destination
def curry(destination, func, argNum):
	called = 0
	args = []
	def curried(arg):
		nonlocal called
		nonlocal args
		called += 1
		args.append(arg)
		if called == argNum:
			return func(*args)
		else:
			return curried
	return curried

# Should get 12
print(curry(None, show1, 1)(23))

# Should get 12
print(curry(None, mul2, 2)(3)(4))

# Should get 12
destination = curry(None, add3, 3)
a = destination(3)
b = a(4)
c = b(5)
print(c)